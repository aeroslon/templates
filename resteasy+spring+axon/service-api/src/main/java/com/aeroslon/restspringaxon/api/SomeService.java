package com.aeroslon.restspringaxon.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/someservice")
public interface SomeService {
	
	@GET
	@Path("/createsomething")
	String createSomething();
	
	@GET
	@Path("/dosomething")
	String doSomething();

}
