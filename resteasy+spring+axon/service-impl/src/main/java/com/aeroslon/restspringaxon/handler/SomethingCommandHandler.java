package com.aeroslon.restspringaxon.handler;

import org.axonframework.commandhandling.annotation.CommandHandler;
import org.axonframework.repository.Repository;

import com.aeroslon.restspringaxon.command.CreateSomethingCommand;
import com.aeroslon.restspringaxon.command.DoSomethingCommand;
import com.aeroslon.restspringaxon.model.SomethingModel;

public class SomethingCommandHandler {
	
	private Repository<SomethingModel> repository;

	public void setRepository(final Repository<SomethingModel> repository) {
		this.repository = repository;
	}

	@CommandHandler
	public void handle(final CreateSomethingCommand command) {
		final SomethingModel model = new SomethingModel(command);
		repository.add(model);
	}
	
	@CommandHandler
	public void handle(final DoSomethingCommand command) {
		final SomethingModel model = repository.load(command.rootAggregateId);
		model.handle(command);
	}

}
