package com.aeroslon.restspringaxon.impl;

import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aeroslon.restspringaxon.api.SomeService;
import com.aeroslon.restspringaxon.command.CreateSomethingCommand;
import com.aeroslon.restspringaxon.command.DoSomethingCommand;

@Component
public class SomeServiceImpl implements SomeService {
	
	private volatile String lastGeneratedRootAggregateId;
	
	@Autowired
	private CommandGateway commandGateway;

	@Override
	public String createSomething() {
		String id = String.valueOf(System.currentTimeMillis());
		CreateSomethingCommand command = new CreateSomethingCommand(id);
		commandGateway.sendAndWait(command);
		lastGeneratedRootAggregateId = id;

		return "Something created: " + lastGeneratedRootAggregateId;
	}
	
	@Override
	public String doSomething() {
		String something = "Something happend at :" + System.currentTimeMillis();
		
		DoSomethingCommand command = new DoSomethingCommand(lastGeneratedRootAggregateId, something);
		commandGateway.sendAndWait(command);		
		
		return something;
	}
	
}
