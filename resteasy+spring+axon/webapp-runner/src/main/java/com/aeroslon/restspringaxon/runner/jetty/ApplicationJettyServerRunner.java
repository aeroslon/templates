package com.aeroslon.restspringaxon.runner.jetty;

import java.io.File;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;

public class ApplicationJettyServerRunner {
	
	
	public static void main(String[] args) throws Exception {
		Server server = new Server(8080);
		WebAppContext context = new WebAppContext();
		context.setContextPath("/");
		
		File file = new File("../webapp/target/something.war");
		context.setWar(file.getAbsolutePath());
		
		server.setHandler(context);
		
		server.start();
		server.join();
	}

}
