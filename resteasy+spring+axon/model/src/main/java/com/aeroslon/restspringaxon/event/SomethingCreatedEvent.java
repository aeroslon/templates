package com.aeroslon.restspringaxon.event;

public class SomethingCreatedEvent extends AbstractEvent {

	private static final long serialVersionUID = 2260232412196245290L;

	public SomethingCreatedEvent(final String rootAggregateId) {
		super(rootAggregateId);
	}

}
