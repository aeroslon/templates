package com.aeroslon.restspringaxon.command;

import java.io.Serializable;

import org.axonframework.commandhandling.annotation.TargetAggregateIdentifier;

public class AbstractCommand implements Serializable {

	private static final long serialVersionUID = -4588447067801885343L;
	
	@TargetAggregateIdentifier
	public final String rootAggregateId;

	public AbstractCommand(final String rootAggregateId) {
		this.rootAggregateId = rootAggregateId;
	}

}
