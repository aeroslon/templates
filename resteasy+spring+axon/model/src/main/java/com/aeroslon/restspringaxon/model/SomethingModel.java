package com.aeroslon.restspringaxon.model;

import org.axonframework.eventhandling.annotation.EventHandler;
import org.axonframework.eventsourcing.annotation.AbstractAnnotatedAggregateRoot;

import com.aeroslon.restspringaxon.command.CreateSomethingCommand;
import com.aeroslon.restspringaxon.command.DoSomethingCommand;
import com.aeroslon.restspringaxon.event.SomethingCreatedEvent;
import com.aeroslon.restspringaxon.event.SomethingHappenedEvent;

public class SomethingModel extends AbstractAnnotatedAggregateRoot<String> {
	
	private String rootAggregateId;
	
	private String someThing;
	
	private static final long serialVersionUID = -2626560323522415038L;
	
	protected SomethingModel() {
	}
	
	public SomethingModel(final CreateSomethingCommand command) {
		apply(new SomethingCreatedEvent(command.rootAggregateId));
	}
	
	public void handle(final DoSomethingCommand command) {
		apply(new SomethingHappenedEvent(command.rootAggregateId, command.someThing));
	}
	
	/** Event Handlers **/
	
	@EventHandler
	public void on(final SomethingCreatedEvent event) {
		this.rootAggregateId = event.rootAggregateId;
	}
	
	@EventHandler
	public void on(final SomethingHappenedEvent event) {
		this.someThing = event.someThing;
	}

	@Override
	public String getIdentifier() {
		return rootAggregateId;
	}

	public String getSomeThing() {
		return someThing;
	}

}
