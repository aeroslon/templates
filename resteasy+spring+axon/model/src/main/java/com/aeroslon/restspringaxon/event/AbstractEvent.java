package com.aeroslon.restspringaxon.event;

import java.io.Serializable;

public class AbstractEvent implements Serializable {
	
	private static final long serialVersionUID = 8895382205975837167L;
	
	public final String rootAggregateId;

	public AbstractEvent(final String rootAggregateId) {
		this.rootAggregateId = rootAggregateId;
	}

}
