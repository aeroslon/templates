package com.aeroslon.restspringaxon.command;

public class DoSomethingCommand extends AbstractCommand {

	private static final long serialVersionUID = -5437770468627325437L;
	
	public final String someThing;
	
	public DoSomethingCommand(final String rootAggregateId,
			final String someThing) {
		super(rootAggregateId);
		this.someThing = someThing;
	}
	
}
