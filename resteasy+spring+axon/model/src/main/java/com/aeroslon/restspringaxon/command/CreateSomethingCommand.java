package com.aeroslon.restspringaxon.command;

public class CreateSomethingCommand extends AbstractCommand {

	private static final long serialVersionUID = 3906986870856937342L;

	public CreateSomethingCommand(final String rootAggregateId) {
		super(rootAggregateId);
	}

}
