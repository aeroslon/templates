package com.aeroslon.restspringaxon.event;

public class SomethingHappenedEvent extends AbstractEvent {

	private static final long serialVersionUID = 732910855450713310L;
	
	public final String someThing;
	
	public SomethingHappenedEvent(final String rootAggregateId, 
			final String someThing) {
		super(rootAggregateId);
		this.someThing = someThing;
	}

}
